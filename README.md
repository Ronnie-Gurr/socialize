# README #

### What is this repository for? ###

Setup App is an app for getting people connected! The app is based upon people's interest such as programming, photography etc!
This repository is for making updates, sharing work and seeing what needs to be done!
Version: V0.1
Join the Discord chat https://discord.gg/exQ4jFF (Check Pin Messages)

### What needs to be done? ###

Concept ideas: First thing we all need to do is get the concept down before we start working on the app!
Designing: Second task is to get the graphic designer (Bryan) to start designing what the app will look like for it's first version.
Building the website: George, Bryan and Ronnie need to build a website for the company.
Getting the word out: Mix (Social media manger), you need to make our Facebook, Twitter page and any other social media sites such as Instagram etc.
Developers: Ronnie and George will need to get in contact with out developers to work on this project!
Building the app: Ronnie, George and any other devs will start working on the app as Bryan finishes the designs.
Advertisement: Mix and Bryan will need to create an advert to gain peoples interest in the app. 
Beta: First beta is aimed to be released by December 2017, the team will can invite 10 friends to test the app.
Feedback: Get the feedback sheet from the beta release and analyse the data.
Improvements: Make updates to the app based on the feedback from the beta feedback.
Release: We are aiming to release version 1 of the app in spring 2018!

### Who do I talk to? ###

For any information on login details please contact a founder, Ronnie, George, Mix or Bryan!
for graphics please contact Bryan or another graphic designer on the team!
for another help please message the founders in Discord!


### George task list ###

1: Get social media icons for website based on the social media sites mix set up!
2: Get get photos of the team for team section on website!
3: tell mix to read this :)s

### Mix task list ###

1: Update description and profile pictures/banners on social media sites!

Ronnie Gurr - 11/06/2017